import React, { useState, useEffect } from "react";
import { Avatar, Button, Snackbar } from "@material-ui/core";
import MuiAlert from "@material-ui/lab/Alert";
import CssBaseline from "@material-ui/core/CssBaseline";
import TextField from "@material-ui/core/TextField";
import FormControlLabel from "@material-ui/core/FormControlLabel";
import Checkbox from "@material-ui/core/Checkbox";
import Link from "@material-ui/core/Link";
import Paper from "@material-ui/core/Paper";
import Box from "@material-ui/core/Box";
import Grid from "@material-ui/core/Grid";
import LockOutlinedIcon from "@material-ui/icons/LockOutlined";
import Typography from "@material-ui/core/Typography";
import { makeStyles, ThemeProvider } from "@material-ui/core/styles";
import ls from "local-storage";
import axios from "axios";
import { Redirect } from "react-router-dom";
import theme from "../Theme";

const useStyles = makeStyles(() => ({
  root: {
    height: "100vh",
  },
  image: {
    backgroundImage: "url(https://source.unsplash.com/random)",
    backgroundRepeat: "no-repeat",
    backgroundColor:
      theme.palette.type === "light"
        ? theme.palette.grey[50]
        : theme.palette.grey[900],
    backgroundSize: "cover",
    backgroundPosition: "center",
  },
  paper: {
    margin: theme.spacing(8, 4),
    display: "flex",
    flexDirection: "column",
    alignItems: "center",
  },
  avatar: {
    margin: theme.spacing(1),
    backgroundColor: theme.palette.primary.main,
  },
  form: {
    width: "100%", // Fix IE 11 issue.
    marginTop: theme.spacing(1),
  },
  submit: {
    margin: theme.spacing(3, 0, 2),
  },
}));

function Alert(props) {
  return <MuiAlert elevation={6} variant="filled" {...props} />;
}

export default function Login(props) {
  const api = "http://localhost:5000/login";
  const classes = useStyles();
  const [username, setUsername] = useState("");
  const [pass, setPass] = useState("");
  const [loggedIn, setloggedIn] = useState(false);
  const [register, setRegister] = useState(false);
  const [alertOpen, setAlertOpen] = useState(false);
  const [type, setType] = useState("error");
  const [notifMessage, setMsg] = useState("Invalid Credentials");

  useEffect(() => {
    if (props.location.state?.newUser) {
      setAlertOpen(true);
      setType("success");
      setMsg("Login with your account");
    }
  }, []);

  const handleSignIn = async () => {
    console.log(username + pass);
    try {
      let res = await axios.post(api, { username: username, password: pass });
      console.log(res);
      ls.set("username", username);
      ls.set("pass", pass);
      ls.set("loggedIn", true);
      setloggedIn(true);
    } catch (e) {
      setType("error");
      setMsg("Invalid Credentials");
      setAlertOpen(true);
      console.error(e);
    }
  };
  const handleReg = () => {
    setRegister(true);
  };
  return (
    <ThemeProvider theme={theme}>
      <Grid container component="main" className={classes.root}>
        {loggedIn && <Redirect to="/home" push></Redirect>}
        {register && <Redirect to="/register" push></Redirect>}
        <CssBaseline />
        <Grid item xs={false} sm={4} md={7} className={classes.image} />
        <Grid
          item
          xs={12}
          sm={8}
          md={5}
          component={Paper}
          elevation={6}
          square
          style={{
            color: theme.palette.primary.main,
          }}
        >
          <div className={classes.paper}>
          <Typography component="h1" variant="h4">
              Welcome to PassProtect
            </Typography>
            <Avatar className={classes.avatar}>
              <LockOutlinedIcon />
            </Avatar>
            <Typography component="h1" variant="h5">
              Sign in
            </Typography>
            <form className={classes.form} noValidate>
              <TextField
                variant="outlined"
                margin="normal"
                required
                fullWidth
                id="username"
                label="Username"
                name="username"
                autoComplete="username"
                autoFocus
                onChange={(text) => setUsername(text.target.value)}
              />
              <TextField
                variant="outlined"
                margin="normal"
                required
                fullWidth
                name="password"
                label="Password"
                type="password"
                id="password"
                autoComplete="current-password"
                onChange={(text) => setPass(text.target.value)}
              />
              <FormControlLabel
                control={<Checkbox value="remember" color="primary" />}
                label="Remember me"
              />
              <Button
                fullWidth
                variant="contained"
                style={{
                  backgroundColor: theme.palette.primary.main,
                  color: "#fff",
                }}
                className={classes.submit}
                onClick={handleSignIn}
              >
                Sign In
              </Button>
              <Grid container>
                <Grid item>
                  <Link
                    onClick={handleReg}
                    variant="body2"
                    style={{
                      color: theme.palette.primary.main,
                      cursor: "pointer"
                    }}
                  >
                    {"Don't have an account? Sign Up"}
                  </Link>
                </Grid>
              </Grid>
              <Box mt={5}></Box>
            </form>
          </div>
          <Snackbar
            open={alertOpen}
            autoHideDuration={6000}
            onClose={() => setAlertOpen(false)}
          >
            <Alert onClose={() => setAlertOpen(false)} severity={type}>
              {notifMessage}
            </Alert>
          </Snackbar>
        </Grid>
      </Grid>
    </ThemeProvider>
  );
}
