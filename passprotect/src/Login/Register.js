import React, { useState } from "react";
import {
  Button,
  CssBaseline,
  TextField,
  Paper,
  Box,
  Grid,
  Link,
  Typography,
  Snackbar,
  Avatar,
} from "@material-ui/core";
import { makeStyles, ThemeProvider } from "@material-ui/core/styles";
import LockOutlinedIcon from "@material-ui/icons/LockOutlined";
import ls from "local-storage";
import axios from "axios";
import { Redirect } from "react-router-dom";
import MuiAlert from "@material-ui/lab/Alert";
import theme from "../Theme";

const useStyles = makeStyles(() => ({
  root: {
    height: "100vh",
  },
  image: {
    backgroundImage: "url(https://source.unsplash.com/random)",
    backgroundRepeat: "no-repeat",
    backgroundColor:
      theme.palette.type === "light"
        ? theme.palette.grey[50]
        : theme.palette.grey[900],
    backgroundSize: "cover",
    backgroundPosition: "center",
  },
  paper: {
    margin: theme.spacing(8, 4),
    display: "flex",
    flexDirection: "column",
    alignItems: "center",
  },
  avatar: {
    margin: theme.spacing(1),
    backgroundColor: theme.palette.primary.main,
  },
  form: {
    width: "100%", // Fix IE 11 issue.
    marginTop: theme.spacing(1),
  },
  submit: {
    margin: theme.spacing(3, 0, 2),
  },
}));

function Alert(props) {
  return <MuiAlert elevation={6} variant="filled" {...props} />;
}
export default function Register() {
  const api = "http://localhost:5000/register";
  const classes = useStyles();
  const [username, setUsername] = useState("");
  const [pass, setPass] = useState("");
  const [registered, setRegistered] = useState(false);
  const [alertOpen, setAlertOpen] = useState(false);

  const handleSignUp = async () => {
    try {
      let res = await axios.post(api, { username: username, password: pass });
      console.log(res);
      ls.set("username", username);
      ls.set("pass", pass);
      ls.set("loggedIn", true);
      setRegistered(true);
    } catch (e) {
      setAlertOpen(true);
      console.error(e);
    }
  };

  return (
    <ThemeProvider theme={theme}>
      <Grid container component="main" className={classes.root}>
        {registered && (
          <Redirect
            to={{ pathname: "/", state: { newUser: true } }}
            push
          ></Redirect>
        )}
        <CssBaseline />
        <Grid item xs={false} sm={4} md={7} className={classes.image} />
        <Grid item xs={12} sm={8} md={5} component={Paper} elevation={6} square>
          <div className={classes.paper}>
            <Avatar className={classes.avatar}>
              <LockOutlinedIcon />
            </Avatar>
            <Typography component="h1" variant="h5">
              Register
            </Typography>
            <form className={classes.form} noValidate>
              <TextField
                variant="outlined"
                margin="normal"
                required
                fullWidth
                id="username"
                label="Username"
                name="username"
                autoComplete="username"
                autoFocus
                onChange={(text) => setUsername(text.target.value)}
              />
              <TextField
                variant="outlined"
                margin="normal"
                required
                fullWidth
                name="password"
                label="Password"
                type="password"
                id="password"
                autoComplete="current-password"
                onChange={(text) => setPass(text.target.value)}
              />
              <Button
                fullWidth
                variant="contained"
                color="primary"
                className={classes.submit}
                onClick={handleSignUp}
              >
                Register Now
              </Button>
              <Box mt={5}></Box>
              <Link
                onClick={() => {
                  setRegistered(true);
                }}
                variant="body2"
              >
                {"Already have an account? Sign In"}
              </Link>
            </form>
          </div>
        </Grid>
        <Snackbar
          open={alertOpen}
          autoHideDuration={6000}
          onClose={() => setAlertOpen(false)}
        >
          <Alert onClose={() => setAlertOpen(false)} severity="error">
            Username exists!
          </Alert>
        </Snackbar>
      </Grid>
    </ThemeProvider>
  );
}
