import React, { useState, useEffect } from "react";
import {
  AppBar,
  Button,
  CssBaseline,
  Toolbar,
  Container,
  Grid,
  Modal,
  Typography,
  Snackbar,
} from "@material-ui/core";
import MuiAlert from "@material-ui/lab/Alert";
import VpnKeyIcon from "@material-ui/icons/VpnKey";
import Toc from "@material-ui/icons/Toc";
import AddIcon from "@material-ui/icons/Add";
import ExitToAppIcon from "@material-ui/icons/ExitToApp";
import { makeStyles } from "@material-ui/core/styles";
import ls from "local-storage";
import axios from "axios";
import { Redirect } from "react-router";
import PasswordModal from "../components/PasswordModal";
import { Drawer } from "baseui/drawer";
import { Display4 } from "baseui/typography";
import { StatefulMenu, OptionProfile } from "baseui/menu";
import Search from "baseui/icon/search";
import { ListItem, ListItemLabel, ARTWORK_SIZES } from "baseui/list";
import PasswordCard from "../components/PasswordPane";

function Alert(props) {
  return <MuiAlert elevation={6} variant="filled" {...props} />;
}

const useStyles = makeStyles((theme) => ({
  root: { flexGrow: 1 },
  title: {
    flexGrow: 1,
  },
  icon: {
    marginRight: theme.spacing(2),
  },
  heroContent: {
    backgroundColor: theme.palette.background.paper,
    padding: theme.spacing(8, 0, 6),
  },
  heroButtons: {
    marginTop: theme.spacing(4),
  },
  cardGrid: {
    transition: 5,
    paddingTop: theme.spacing(8),
    paddingBottom: theme.spacing(8),
  },
  footer: {
    backgroundColor: theme.palette.background.paper,
    padding: theme.spacing(6),
  },
  mainflex: {
    display: "flex",
    flexDirection: "row",
    justifyContent: "center",
    alignItems: "center",
  },
  myflexRow: {
    display: "flex",
    flexDirection: "column",
    justifyContent: "flex-start",
    alignItems: "flex-start",
    height: "60vh",
    width: "50vw",
    backgroundColor: "black",
  },
  grid2: {
    backgroundColor: "#333",
    height: "60vh",
    width: "50vw",
  },
}));

export default function Home() {
  const api = "http://localhost:5000";

  const [card, setCardOpen] = useState("pass");
  const [data, setData] = useState([]);
  const [modalOpen, setModalOpen] = useState(false);
  const [newPassword, setNewPassword] = useState({});
  const [trigger, setTrigger] = useState(false);
  const [alertOpen, setAlertOpen] = useState(false);
  const [isOpen, setIsOpen] = React.useState(false);
  const [next, setNext] = React.useState("");
  const [grid, setGrid] = React.useState("");
  const classes = useStyles();

  const fetchData = async () => {
    if (ls.get("loggedIn")) {
      try {
        let res = await axios.post(`${api}/getPass`, {
          username: ls.get("username"),
          password: "123", // Change needed
        });
        setData(res.data);
      } catch (e) {
        console.error(e);
      }
    }
  };
  useEffect(() => {
    fetchData();
  }, [modalOpen, trigger]);

  const items = [
    {
      title: "My Passwords",
      subtitle: "Show the list of my saved passwords",
      imgUrl: "https://via.placeholder.com/60x60",
    },
    {
      title: "My Credit Cards",
      subtitle: "Show the list of my credit card info",
      imgUrl: "https://via.placeholder.com/60x60",
    },
    {
      title: "My Account",
      subtitle: "Manage your user account",
      imgUrl: "https://via.placeholder.com/60x60",
    },
  ];

  return (
    <React.Fragment>
      {!ls.get("loggedIn") && <Redirect to="/"></Redirect>}
      {next === "My Passwords" && <Redirect to="/mypass" push></Redirect>}

      <div className={classes.root}>
        <CssBaseline />
        <Drawer isOpen={isOpen} autoFocus onClose={() => setIsOpen(false)}>
          <div style={{ marginTop: 50 }}>
            <Display4 style={{ marginBottom: 50 }}>
              Welcome to PassProtect
            </Display4>
            <StatefulMenu
              items={items}
              onItemSelect={(e) => {
                console.log(e);
                setNext(e.item.title);
              }}
              overrides={{
                List: {
                  style: {
                    width: "420px",
                  },
                },
                Option: {
                  component: OptionProfile,
                  props: {
                    getProfileItemLabels: ({ title, subtitle }) => ({
                      title,
                      subtitle,
                    }),
                    getProfileItemImg: (item) => item.imgUrl,
                    getProfileItemImgText: (item) => item.title,
                  },
                },
              }}
            />
          </div>
        </Drawer>
        <AppBar position="relative">
          <Toolbar>
            <Toc
              onClick={() => setIsOpen(!isOpen)}
              className={classes.icon}
              style={{ cursor: "pointer" }}
            ></Toc>
            <VpnKeyIcon className={classes.icon} />
            <Typography
              variant="h6"
              color="inherit"
              className={classes.title}
              noWrap
            >
              PassProtect
            </Typography>
            <Button
              variant="outlined"
              color="inherit"
              startIcon={<ExitToAppIcon />}
              onClick={() => {
                console.log(ls.get("loggedIn") || 0);
                ls.clear();
                window.location.reload(false);
              }}
            >
              Logout
            </Button>
          </Toolbar>
        </AppBar>
      </div>
      <main>
        {/* Hero unit */}
        <div className={classes.heroContent}>
          <Container maxWidth="sm">
            <Typography
              component="h1"
              variant="h2"
              align="center"
              color="textPrimary"
              gutterBottom
            >
              PassProtect
            </Typography>
            <Typography
              variant="h5"
              align="center"
              color="textSecondary"
              paragraph
            >
              Store all your passwords in one place. Securely.
            </Typography>
            <div className={classes.heroButtons}>
              <Grid container spacing={2} justify="center">
                <Grid item>
                  <Button
                    variant="contained"
                    color="primary"
                    startIcon={<AddIcon />}
                    onClick={() => {
                      setModalOpen(true);
                      setCardOpen("pass");
                    }}
                  >
                    Add a password
                  </Button>
                  <Button
                    variant="contained"
                    color="primary"
                    startIcon={<AddIcon />}
                    onClick={() => {
                      setModalOpen(true);
                      setCardOpen("card");
                    }}
                  >
                    Add a Credit Card
                  </Button>
                  <Modal
                    open={modalOpen}
                    onClose={() => {
                      setModalOpen(false);
                    }}
                    style={{
                      display: "flex",
                      justifyContent: "center",
                      alignItems: "center",
                    }}
                  >
                    <PasswordModal
                      setModalOpen={setModalOpen}
                      newPassword={newPassword}
                      setNewPassword={setNewPassword}
                      setAlertOpen={setAlertOpen}
                      modalType={card}
                    />
                  </Modal>
                </Grid>
              </Grid>
            </div>
          </Container>
        </div>

        <div className={classes.mainflex}>
          <div className={classes.myflexRow}>
            <Display4 margin={20} color="white">
              My PassWords
            </Display4>
            {data.length &&
              data.map((val) => {
                return (
                  <ListItem artwork={Search} artworkSize={ARTWORK_SIZES.LARGE}>
                    <ListItemLabel>
                      <Button
                        onClick={() => {
                          setGrid(val);
                        }}
                        variant="contained"
                        color="primary"
                      >
                        {val.title}
                      </Button>
                    </ListItemLabel>
                  </ListItem>
                );
              })}
          </div>
          <div className={classes.grid2}>
            <PasswordCard
              trigger={trigger}
              setTrigger={setTrigger}
              setAlertOpen={setAlertOpen}
              data={grid}
            />
          </div>
        </div>

        <Snackbar
          open={alertOpen}
          autoHideDuration={6000}
          onClose={() => setAlertOpen(false)}
        >
          <Alert onClose={() => setAlertOpen(false)} severity="success">
            Successful!
          </Alert>
        </Snackbar>
      </main>
    </React.Fragment>
  );
}
