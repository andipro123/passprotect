import React, { useEffect, useState } from "react";
import { makeStyles, ThemeProvider } from "@material-ui/core/styles";
import Sidebar from "../components/Sidebar";
import { CssBaseline, Grid, Modal } from "@material-ui/core";
import PasswordModal from "../components/PasswordModal";
import ListPane from "../components/ListPane";
import PasswordPane from "../components/PasswordPane";
import theme from "../Theme";
import { Redirect } from "react-router";
import ls from "local-storage";

const useStyles = makeStyles((theme) => ({
  toolbar: {
    display: "flex",
    alignItems: "center",
    justifyContent: "flex-end",
    padding: theme.spacing(0, 1),
    // necessary for content to be below app bar
    ...theme.mixins.toolbar,
  },
  content: {
    flexGrow: 1,
    marginLeft: 240,
    padding: theme.spacing(3),
  },
}));

export default function MiniDrawer() {
  const classes = useStyles();
  const [page, setPage] = useState("personal");
  const [open, setOpen] = React.useState(false);
  const [selectedData, setSelectedData] = useState({});
  const [modalOpen, setModalOpen] = useState(false);
  const [newPassword, setNewPassword] = useState({});
  const [card, setCardOpen] = useState("pass");
  const [alertOpen, setAlertOpen] = useState(false);
  const [trigger, setTrigger] = useState(false);
  const [show, setShow] = React.useState(false);
  
  useEffect(() => {
    console.log("refreshing...");
  }, [trigger]);
  return (
    <ThemeProvider theme={theme}>
      {!ls.get("loggedIn") && <Redirect to="/"></Redirect>}
      <div
        className={classes.root}
        style={{
          backgroundColor: theme.palette.primary.dark,
          color: "#fff",
        }}
      >
        <CssBaseline />
        <Sidebar open={open} setOpen={setOpen} setPage={setPage} />
        <Modal
          open={modalOpen}
          onClose={() => {
            setModalOpen(false);
          }}
          style={{
            display: "flex",
            justifyContent: "center",
            alignItems: "center",
          }}
        >
          <PasswordModal
            setModalOpen={setModalOpen}
            newPassword={newPassword}
            setNewPassword={setNewPassword}
            setAlertOpen={setAlertOpen}
            modalType={card}
            trigger={trigger}
            setTrigger={setTrigger}
          />
        </Modal>
        <Grid container spacing={0}>
          <Grid item xs={3}>
            <ListPane
              sidebar={open}
              page={page}
              setSelectedData={setSelectedData}
              setModalOpen={setModalOpen}
              setCardOpen={setCardOpen}
              trigger={trigger}
              setTrigger={setTrigger}
              setShow={setShow}
            />
          </Grid>
          <Grid item xs={9}>
            <PasswordPane
              data={selectedData}
              sidebar={open}
              setTrigger={setTrigger}
              trigger={trigger}
              setShow={setShow}
              show={show}
            />
          </Grid>
        </Grid>
      </div>
    </ThemeProvider>
  );
}
