import React from "react";
import {
  Grid,
  Typography,
  Paper,
  Button,
  List,
  ListItemText,
  ListItem,
  ListItemSecondaryAction,
  Snackbar,
} from "@material-ui/core";
import DeleteIcon from "@material-ui/icons/Delete";
import { makeStyles, ThemeProvider } from "@material-ui/core/styles";
import ls from "local-storage";
import axios from "axios";
import FileCopyIcon from "@material-ui/icons/FileCopy";
import Visibility from "@material-ui/icons/Visibility";
import VisibilityOff from "@material-ui/icons/VisibilityOff";
import WebIcon from "@material-ui/icons/Web";
import AssignmentIndIcon from "@material-ui/icons/AssignmentInd";
import IconButton from "@material-ui/core/IconButton";
import LockIcon from "@material-ui/icons/Lock";
import theme from "../Theme";
import MuiAlert from "@material-ui/lab/Alert";
import Dialog from "@material-ui/core/Dialog";
import DialogActions from "@material-ui/core/DialogActions";
import DialogContent from "@material-ui/core/DialogContent";
import DialogContentText from "@material-ui/core/DialogContentText";
import DialogTitle from "@material-ui/core/DialogTitle";
import { Input } from "baseui/input";
import Cards from "react-credit-cards";
import "react-credit-cards/es/styles-compiled.css";

const useStyles = makeStyles((theme) => ({
  root: {
    width: "70%",
    marginTop: theme.spacing(7),
    height: "90vh",
    paddingTop: 30,
    alignItems: "center",
    justifyContent: "center",
  },
  paper: {
    padding: 20,
  },
}));
function Alert(props) {
  return <MuiAlert elevation={6} variant="filled" {...props} />;
}
const PasswordPane = ({
  trigger,
  setTrigger,
  data,
  setAlertOpen,
  sidebar,
  setShow,
  show,
}) => {
  const classes = useStyles();
  const [passVisible, setPassVisible] = React.useState(false);
  const [NotifOpen, setNotifOpen] = React.useState(false);
  const [NotifType, setNotifType] = React.useState("success");
  const [delconfirm, setDelconfirm] = React.useState(false);
  const [delconfirm1, setDelconfirm1] = React.useState(false);
  const [msg, setmsg] = React.useState("Copied to clipboard :)");
  const [master, setMaster] = React.useState("");

  const api = "http://localhost:5000";
  const validatepass = async (pass) => {
    try {
      let res = await axios.post(`${api}/validatepass`, {
        username: ls.get("username"),
        pass: pass,
      });
      if (res.status === 200) {
        setmsg("Validated successfully");
        setNotifType("success");
        setNotifOpen(true);
        setPassVisible(true);
      }
    } catch (e) {
      setmsg("Invalid master password");
      setNotifType("error");
      setNotifOpen(true);
    }
  };
  const handleDeletePassword = async (url) => {
    try {
      if (ls.get("loggedIn")) {
        let res = await axios.post(`${api}/deletepass`, {
          username: ls.get("username"),
          url: url,
        });
        if (res.status === 200) {
          console.log("deleted!");
          setNotifOpen(true);
          setmsg("Deleted password");
          setTrigger(!trigger);
          setShow(false);
        } else {
          setNotifType("error");
          setmsg("Error while deleting password");
          setNotifOpen(true);
        }
      }
    } catch (e) {
      console.error(e);
    }
  };
  return (
    <ThemeProvider theme={theme}>
      <div
        style={sidebar ? { marginLeft: 239 } : { marginLeft: 150 }}
        className={classes.root}
      >
        <Paper
          elevation={5}
          className={classes.paper}
          square
          style={{
            backgroundColor: theme.palette.primary.main,
            color: "#fff",
          }}
        >
          {show && data && (
            <>
              <Dialog
                open={delconfirm}
                onClose={() => {
                  setDelconfirm(false);
                }}
                aria-labelledby="alert-dialog-title"
                aria-describedby="alert-dialog-description"
              >
                <DialogTitle id="alert-dialog-title">
                  {"Delete Password?"}
                </DialogTitle>
                <DialogContent>
                  <DialogContentText id="alert-dialog-description">
                    Are you sure you want to delete this password? This action
                    cannot be undone
                  </DialogContentText>
                </DialogContent>
                <DialogActions>
                  <Button
                    onClick={() => {
                      setDelconfirm(false);
                    }}
                    color="primary"
                  >
                    No
                  </Button>
                  <Button
                    onClick={() => {
                      handleDeletePassword(data.siteURL);
                      setDelconfirm(false);
                    }}
                    color="primary"
                    autoFocus
                  >
                    Yes
                  </Button>
                </DialogActions>
              </Dialog>

              <Dialog
                open={delconfirm1}
                onClose={() => {
                  setDelconfirm1(false);
                }}
                aria-labelledby="alert-dialog-title"
                aria-describedby="alert-dialog-description"
              >
                <DialogTitle id="alert-dialog-title">
                  {"Enter Master Password"}
                </DialogTitle>
                <DialogContent>
                  <img
                    src="https://unotechsoft.com/wp-content/uploads/2020/06/800X800px_Multi-factor-Authentication.gif"
                    alt="auth"
                    height="200px"
                    width="180px"
                    style={{
                      display: "block",
                      marginLeft: "auto",
                      marginRight: "auto",
                    }}
                  ></img>
                  <DialogContentText id="alert-dialog-description">
                    Type your master password to confirm
                  </DialogContentText>
                  <Input
                    type="password"
                    placeholder="Master Password"
                    onChange={(e) => setMaster(e.target.value)}
                  />
                </DialogContent>
                <DialogActions>
                  <Button
                    onClick={() => {
                      setDelconfirm1(false);
                    }}
                    color="primary"
                  >
                    Cancel
                  </Button>
                  <Button
                    onClick={async () => {
                      await validatepass(master);
                      setDelconfirm1(false);
                    }}
                    color="primary"
                    autoFocus
                  >
                    Validate Password
                  </Button>
                </DialogActions>
              </Dialog>

              <Grid container spacing={2}>
                <Grid item xs={9}>
                  <Typography variant="h4">{data.title}</Typography>
                </Grid>

                <Grid item xs={12}>
                  {data.passtype === "creditcard" ? (
                    <Cards
                      name={data.username}
                      number={data.siteURL}
                      expiry="mm/yy"
                      cvc={data.pass}
                      focused={passVisible && "cvc"}
                    />
                  ) : (
                    <List>
                      <ListItem
                        divider
                        style={{ backgroundColor: theme.palette.primary.light }}
                      >
                        <IconButton>
                          <WebIcon style={{ fontSize: 30, color: "#fff" }} />
                        </IconButton>
                        <ListItemText
                          primary={
                            data.passtype === "creditcard"
                              ? "Card Number"
                              : "Website"
                          }
                        />
                        <ListItemText primary={data.siteURL} />
                        <ListItemSecondaryAction>
                          <IconButton
                            edge="end"
                            aria-label="Copy"
                            onClick={() => {
                              navigator.clipboard.writeText(data.siteURL);
                              setmsg("Copied to clipboard :)");
                              setNotifOpen(true);
                            }}
                          >
                            <FileCopyIcon style={{ color: "#fff" }} />
                          </IconButton>
                        </ListItemSecondaryAction>
                      </ListItem>
                      <ListItem
                        divider
                        style={{ backgroundColor: theme.palette.primary.light }}
                      >
                        <IconButton>
                          <AssignmentIndIcon
                            style={{ fontSize: 30, color: "#fff" }}
                            color="primary"
                          />
                        </IconButton>
                        <ListItemText
                          primary={
                            data.passtype === "creditcard"
                              ? "Holder Name"
                              : "Username"
                          }
                        />
                        <ListItemText primary={data.username} />
                        <ListItemSecondaryAction>
                          <IconButton
                            edge="end"
                            aria-label="Copy"
                            onClick={() => {
                              navigator.clipboard.writeText(data.username);
                              setmsg("Copied to clipboard :)");
                              setNotifOpen(true);
                            }}
                          >
                            <FileCopyIcon style={{ color: "#fff" }} />
                          </IconButton>
                        </ListItemSecondaryAction>
                      </ListItem>
                      {passVisible ? (
                        <ListItem
                          divider
                          style={{
                            backgroundColor: theme.palette.primary.light,
                          }}
                        >
                          <IconButton>
                            <LockIcon style={{ fontSize: 30, color: "#fff" }} />
                          </IconButton>
                          <ListItemText primary="Password" />
                          <ListItemText primary={data.pass} />
                          <ListItemSecondaryAction>
                            <IconButton
                              edge="end"
                              aria-label="Copy"
                              onClick={() => setPassVisible(false)}
                            >
                              <Visibility style={{ color: "#fff" }} />
                            </IconButton>
                          </ListItemSecondaryAction>
                        </ListItem>
                      ) : (
                        <ListItem
                          divider
                          style={{
                            backgroundColor: theme.palette.primary.light,
                          }}
                        >
                          <IconButton>
                            <LockIcon style={{ fontSize: 30, color: "#fff" }} />
                          </IconButton>
                          <ListItemText primary="Password" />
                          <ListItemText primary="****" />
                          <ListItemSecondaryAction>
                            <IconButton
                              edge="end"
                              aria-label="Copy"
                              onClick={() => {
                                setDelconfirm1(true);
                              }}
                            >
                              {/* <VisibilityOff style={{ color: "#fff" }} /> */}
                            </IconButton>
                          </ListItemSecondaryAction>
                        </ListItem>
                      )}
                    </List>
                  )}
                </Grid>
                <Grid
                  item
                  xs={12}
                  style={{
                    display: "flex",
                    flexDirection: "row",
                    alignItems: "center",
                    justifyContent: "space-between",
                  }}
                >
                  <Button
                    size="small"
                    color="default"
                    style={{ color: theme.palette.error.main }}
                    startIcon={<DeleteIcon />}
                    onClick={() => {
                      setDelconfirm(true);
                    }}
                  >
                    Delete
                  </Button>
                  {passVisible ? (
                    <Button
                      onClick={() => setPassVisible(false)}
                      style={{ color: "#fff" }}
                      startIcon={<Visibility style={{ color: "#fff" }} />}
                    >
                      Hide PIN
                    </Button>
                  ) : (
                    <Button
                      onClick={() => {
                        setDelconfirm1(true);
                      }}
                      style={{ color: "#fff" }}
                      startIcon={<VisibilityOff style={{ color: "#fff" }} />}
                    >
                      Show PIN
                    </Button>
                  )}
                </Grid>
              </Grid>
            </>
          )}
        </Paper>
      </div>
      <Snackbar
        open={NotifOpen}
        autoHideDuration={6000}
        onClose={() => setNotifOpen(false)}
      >
        <Alert severity={NotifType} onClose={() => setNotifOpen(false)}>
          {msg}
        </Alert>
      </Snackbar>
    </ThemeProvider>
  );
};
export default PasswordPane;
