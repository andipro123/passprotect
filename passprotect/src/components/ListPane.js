import React, { useState, useEffect } from "react";
import { makeStyles, ThemeProvider } from "@material-ui/core/styles";
import {
  Typography,
  List,
  ListItem,
  ListItemIcon,
  ListItemText,
  Button,
  Divider,
} from "@material-ui/core";
import { Twitter, StoreMallDirectoryOutlined } from "@material-ui/icons";
import AddIcon from "@material-ui/icons/Add";
import theme from "../Theme";

import ls from "local-storage";
import axios from "axios";

const useStyles = makeStyles((theme) => ({
  root: {
    width: "90%",
    marginTop: theme.spacing(7),
    height: "100vh",
    paddingTop: 30,
    paddingLeft: 20,
    paddingRight: 20,
    marginRight: 0,
    boxShadow: "300px",
    // zIndex: theme.zIndex.tooltip + 1,
  },
}));

const api = "http://localhost:5000";

export default function ListPane({
  sidebar,
  setSelectedData,
  page,
  setModalOpen,
  setCardOpen,
  trigger,
  setTrigger,
  setShow,
}) {
  const classes = useStyles();
  const [data, setData] = useState([]);
  const fetchData = async () => {
    if (ls.get("loggedIn")) {
      try {
        let res = await axios.post(`${api}/getPass`, {
          username: ls.get("username"),
          password: "dada", // Change needed
        });
        setData(res.data);
        console.log(res.data);
      } catch (e) {
        console.error(e);
      }
    }
  };
  useEffect(() => {
    fetchData();
  }, [trigger]);
  return (
    <ThemeProvider theme={theme}>
      <div
        className={classes.root}
        style={
          sidebar
            ? {
                marginLeft: 239,
                backgroundColor: theme.palette.primary.light,
                color: "#fff",
              }
            : {
                marginLeft: 72,
                backgroundColor: theme.palette.primary.light,
                color: "#fff",
              }
        }
      >
        <Typography variant="h6" align="center">
          {page === "personal" ? "Personal Passwords" : "Cards"}
          <Divider />
        </Typography>
        {data.length ? (
          data.map((e, i) => {
            if (page === "personal" && e.passtype === "creditcard") {
              return <></>;
            }
            if (page !== "personal" && e.passtype === "site") {
              return <></>;
            }
            return (
              <List>
                {page === "personal" && e.passtype === "site" && (
                  <ListItem
                    button
                    onClick={() => {
                      setShow(true);
                      setSelectedData(e);
                    }}
                  >
                    <ListItemIcon>
                      {e.title === "Twitter" ? (
                        <Twitter style={{ color: "#fff" }} />
                      ) : (
                        <StoreMallDirectoryOutlined style={{ color: "#fff" }} />
                      )}
                    </ListItemIcon>
                    <ListItemText primary={e.title} />
                  </ListItem>
                )}
                {page === "cards" && e.passtype === "creditcard" && (
                  <ListItem
                    button
                    onClick={() => {
                      setShow(true);
                      setSelectedData(e);
                    }}
                  >
                    <ListItemIcon>
                      {e.title === "Twitter" ? (
                        <Twitter style={{ color: "#fff" }} />
                      ) : (
                        <StoreMallDirectoryOutlined style={{ color: "#fff" }} />
                      )}
                    </ListItemIcon>
                    <ListItemText primary={e.title} />
                  </ListItem>
                )}
              </List>
            );
          })
        ) : (
          <></>
        )}
        {page === "personal" && (
          <Button
            variant="contained"
            color="primary"
            fullWidth
            startIcon={<AddIcon />}
            onClick={() => {
              setModalOpen(true);
              setCardOpen("pass");
            }}
            style={{ marginTop: 10 }}
          >
            Add a password
          </Button>
        )}
        {page === "cards" && (
          <Button
            variant="contained"
            color="primary"
            fullWidth
            startIcon={<AddIcon />}
            onClick={() => {
              setModalOpen(true);
              setCardOpen("card");
            }}
            style={{ marginTop: 10 }}
          >
            Add a Card
          </Button>
        )}
      </div>
    </ThemeProvider>
  );
}
