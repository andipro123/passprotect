import React from "react";
import { Typography, TextField, Button } from "@material-ui/core";
import { makeStyles, ThemeProvider } from "@material-ui/core/styles";
import axios from "axios";
import ls from "local-storage";
import { ModalHeader } from "baseui/modal";
import { PaymentCard } from "baseui/payment-card";
import { PinCode } from "baseui/pin-code";
import { Input } from "baseui/input";
import theme from "../Theme";

const useStyles = makeStyles((theme) => ({
  form: {
    paddingTop: theme.spacing(2),
  },
  paper: {
    display: "flex",
    flexDirection: "column",
    width: 600,
    alignItems: "center",
    justifyContent: "center",
    boxShadow: theme.shadows[5],
    padding: theme.spacing(2, 4, 3),
  },
  text: {
    marginTop: 25,
    width: "100%",
  },
  text1: {
    width: "100%",
    color: "white",
  },
  submit: {
    margin: theme.spacing(3, 0, 2),
  },
  marginclass: {
    marginTop: "20px",
  },
}));

export default function PasswordModal({
  setModalOpen,
  newPassword,
  setNewPassword,
  setAlertOpen,
  modalType,
  trigger,
  setTrigger,
}) {
  const classes = useStyles();
  const api = "http://localhost:5000/addpass";
  const handleAddPassword = async () => {
    const uName = await ls.get("username");
    if (ls.get("loggedIn")) {
      try {
        console.log(uName);
        await setNewPassword({ username: uName, ...newPassword });
        newPassword["username"] = uName;
        console.log(newPassword);
        const res = await axios.post(api, newPassword);
        console.log(res);
        if (res.status === 200) {
          console.log("Password Added!");
          setTrigger(!trigger);
          setModalOpen(false);
        } else {
          console.log("Error");
        }
      } catch (e) {
        console.error(e);
      }
    }
  };
  const [values, setValues] = React.useState(["", "", "", ""]);
  const [cardNo, setCardNo] = React.useState("");
  if (modalType === "card") {
    return (
      <ThemeProvider theme={theme}>
        <div
          className={classes.paper}
          style={{
            display: "flex",
            flexDirection: "column",
            backgroundColor: theme.palette.primary.main,
            height: "60vh",
            color: "white",
          }}
        >
          <h1>Add a Credit Card</h1>

          <Input
            placeholder="Title"
            label="Title"
            onChange={(e) =>
              setNewPassword({ ...newPassword, title: e.target.value })
            }
          />
          <Input
            placeholder="Username"
            className={classes.text}
            onChange={(e) =>
              setNewPassword({ ...newPassword, siteUsername: e.target.value })
            }
          />
          <PaymentCard
            value={cardNo}
            onChange={(e) => {
              setCardNo(e.target.value);
              setNewPassword({ ...newPassword, url: e.target.value });
            }}
            clearOnEscape
            placeholder="Please enter your credit card number..."
          />
          <ModalHeader>Enter Pin</ModalHeader>
          <PinCode
            values={values}
            onChange={({ values }) => {
              setValues(values);
              setNewPassword({ ...newPassword, password: values.join("") });
            }}
            clearOnEscape
            mask
            positive
          />
          <Button
            variant="contained"
            style={{ marginTop: 25 }}
            onClick={() => {
              newPassword["passtype"] = "creditcard";
              handleAddPassword();
            }}
          >
            Add
          </Button>
        </div>
      </ThemeProvider>
    );
  } else {
    return (
      <ThemeProvider theme={theme}>
        <div
          className={classes.paper}
          style={{
            display: "flex",
            flexDirection: "column",
            backgroundColor: theme.palette.primary.main,
            height: "60vh",
            color: "white",
          }}
        >
          <h1>Add a Personal Password</h1>

          <Input
            placeholder="Title"
            label="Title"
            onChange={(e) =>
              setNewPassword({ ...newPassword, title: e.target.value })
            }
          />
          <Input
            placeholder="Website"
            className={classes.text}
            onChange={(e) =>
              setNewPassword({ ...newPassword, url: e.target.value })
            }
          />
          <Input
            placeholder="Username"
            className={classes.text}
            onChange={(e) =>
              setNewPassword({ ...newPassword, siteUsername: e.target.value })
            }
          />
          <Input
            placeholder="Password"
            className={classes.text}
            type="password"
            onChange={(e) =>
              setNewPassword({ ...newPassword, password: e.target.value })
            }
          />
          <Button
            variant="contained"
            style={{ marginTop: 25 }}
            onClick={() => {
              newPassword["passtype"] = "site"
              handleAddPassword();
            }}
          >
            Add
          </Button>
        </div>
      </ThemeProvider>
      // <div className={classes.paper}>
      //   <Typography variant="h6" align="center" color="textPrimary">
      //     Create a new password
      //   </Typography>
      //   <form className={classes.form} noValidate>
      //     <TextField
      //       placeholder="Title"
      //       label="Title"
      //       variant="outlined"
      //       className={classes.text}
      //       onChange={(e) =>
      //         setNewPassword({ ...newPassword, title: e.target.value })
      //       }
      //     />
      //     <TextField
      //       placeholder="Tomain"
      //       label="Domain"
      //       variant="outlined"
      //       className={classes.text}
      //       onChange={(e) =>
      //         setNewPassword({ ...newPassword, url: e.target.value })
      //       }
      //     />
      //     <TextField
      //       placeholder="Tsername"
      //       label="Username"
      //       variant="outlined"
      //       className={classes.text}
      //       onChange={(e) =>
      //         setNewPassword({ ...newPassword, siteUsername: e.target.value })
      //       }
      //     />
      //     <TextField
      //       placeholder="Tass"
      //       label="Password"
      //       type="password"
      //       variant="outlined"
      //       className={classes.text}
      //       onChange={(e) =>
      //         setNewPassword({ ...newPassword, password: e.target.value })
      //       }
      //     />
      //   </form>
      //   <Button
      //     variant="contained"
      //     color="primary"
      //     className={classes.submit}
      //     onClick={handleAddPassword}
      //   >
      //     Add Password
      //   </Button>
      // </div>
    );
  }
}
