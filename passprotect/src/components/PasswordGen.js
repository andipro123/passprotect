import React, { Component } from "react";
import { Button } from "baseui/button";

export default class PasswordGen extends Component {
  constructor(props) {
    super(props);
    this.state = {
      pass: "",
      gen: false,
    };
  }
  genPass = (len = 8) => {
    let length = len;
    let charset =
      "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789!@#$%^&*()";
    let retVal = "";
    for (var i = 0, n = charset.length; i < length; ++i) {
      retVal += charset.charAt(Math.floor(Math.random() * n));
    }
    this.setState({
      pass: retVal,
      gen: true,
    });
  };
  render() {
    return (
      <div>
        <Button kind="secondary" onClick={() => this.genPass()}>
          Generate a password
        </Button>
        {this.state.gen && <h1>Generated password is {this.state.pass}</h1>}
      </div>
    );
  }
}
