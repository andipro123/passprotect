import "./App.css";
import Login from "../Login/Login";
import Home from "../Home/Home";
import Home2 from "../Home/Home2";
import Register from "../Login/Register";
import { Route, BrowserRouter, Switch } from "react-router-dom";
import { Client as Styletron } from "styletron-engine-atomic";
import { Provider as StyletronProvider } from "styletron-react";
import { DarkTheme, BaseProvider, styled } from "baseui";
import ListPane from "../components/ListPane";
const engine = new Styletron();
const Centered = styled("div", {
  height: "100%",
  width: "100%",
});
function App() {
  return (
    <StyletronProvider value={engine}>
      <BaseProvider theme={DarkTheme}>
        <Centered>
          <BrowserRouter>
            <Switch>
              <Route exact path="/home" render={() => <Home2 />}></Route>
              <Route
                exact
                path="/"
                render={(props) => <Login {...props} />}
              ></Route>
              <Route exact path="/register" render={() => <Register />}></Route>
              <Route
                exact
                path="/mypass"
                render={(type) => <ListPane type={type} />}
              ></Route>
            </Switch>
          </BrowserRouter>
        </Centered>
      </BaseProvider>
    </StyletronProvider>
  );
}

export default App;
