import { createMuiTheme } from "@material-ui/core/styles";

const theme = createMuiTheme({
  palette: {
    primary: {
      main: "#2a2a2d",
      light: "#555557",
      dark: "",
    },
    secondary: {
      main: "#4657DF",
      light: "#D6D6E9",
    },
  },
});

export default theme;
