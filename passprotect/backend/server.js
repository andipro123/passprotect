const express = require("express");
const cors = require("cors");
const connectDB = require("./db")
const router = require("./routes")
const port = process.env.PORT || 5000
const app = express();

app.use(express.json({ limit: '50mb' }));
app.use(cors());
app.use(express.urlencoded({ limit: '50mb' }));
app.use("/",router)

app.listen( port, () => {
  console.log(`App listening on ${port}`);
});
(async () => {
    await connectDB();
  })();

