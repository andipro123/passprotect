const mongoose = require("mongoose");

const password = mongoose.Schema(
  {
    title : {
      type : String,
      // required : true
    },
    siteURL : {
      type : String,
      // required : true
    },
    pass : {
      type : String,
      // required : true
    },
    username : {
      type : String,
      // required : true
    },
    passtype : {
      type : String,
      enum : ["site", "creditcard"]
    }
  }
)
const user = mongoose.Schema(
  {
    username: {
      type: String,
      required : true
    },
    password: {
      type: String,
      required : true
    },
    passList: {
      type: [password],
      required : false
    }
  },
  { strict: false }
);


module.exports = {
  user : mongoose.model("user", user),
  pass : mongoose.model("pass",password)
}
