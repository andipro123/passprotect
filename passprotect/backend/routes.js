const router = require("express").Router();
const { user, pass } = require("./models/user");
const crypto = require("crypto");
const bcrypt = require("bcryptjs");

const ENCRYPTION_KEY = process.env.ENCRYPTION_KEY;
const IV_LEN = 16;

router.get("/", async (req, res) => {
  res.send("Welcome to pass protect");
});

const encrypt = (password) => {
  let iv = crypto.randomBytes(IV_LEN);
  let cipher = crypto.createCipheriv(
    "aes-256-cbc",
    Buffer.from(ENCRYPTION_KEY),
    iv
  );
  let encrypted = cipher.update(password);
  encrypted = Buffer.concat([encrypted, cipher.final()]);
  encrypted = iv.toString("hex") + ":" + encrypted.toString("hex");

  return encrypted;
};

const decrypt = (text) => {
  let textParts = text.split(":");
  let iv = Buffer.from(textParts.shift(), "hex");
  let encryptedText = Buffer.from(textParts.join(":"), "hex");
  let decipher = crypto.createDecipheriv(
    "aes-256-cbc",
    Buffer.from(ENCRYPTION_KEY),
    iv
  );
  let decrypted = decipher.update(encryptedText);

  decrypted = Buffer.concat([decrypted, decipher.final()]);

  return decrypted.toString();
};

router.post("/addpass", async (req, res) => {
  console.log(req.body);
  const { username, url, siteUsername, password, title, passtype } = req.body;
  console.log(username, url, siteUsername, password, title);
  // encrypt the password;
  console.log(username, siteUsername);
  let encrypted = encrypt(password);
  try {
    // const data = { url: url, username: siteUsername, password: encrypted };
    const data = new pass({
      title: title,
      siteURL: url,
      pass: encrypted,
      username: siteUsername,
      passtype: passtype
    });
    let update = await user.findOneAndUpdate(
      { username: username },
      { $push: { passList: data } }
    );
    console.log(update);
    return res.status(200).json("Password Added!");
  } catch (e) {
    console.error(e);
    return res.json("Error occurred!");
  }
});

router.post("/validatepass", async (req, res) => {
  const { username, pass } = req.body;
  console.log(username, pass);
  try {
    const curUser = await user.findOne({ username: username });
    if (!curUser) {
      return res.status(404).json("User not found");
    }
    let auth = await bcrypt.compare(pass, curUser.password);
    console.log(auth);
    if (auth) {
      return res.status(200).json("Valid");
    } else {
      return res.status(404).json("Invalid");
    }
  } catch (e) {
    console.log(e);
    return res.status(404).json("Invalid");
  }
});

router.post("/deletepass", async (req, res) => {
  const { username, url } = req.body;
  console.log(username, url);
  try {
    const curUser = await user.findOne({ username: username });
    if (!curUser) {
      return res.status(404).json("User not found");
    }
    let passList = curUser.passList;
    if (
      passList.find((e, i) => {
        if (e.siteURL === url) {
          passList.splice(i, 1);
          return true;
        }
        return false;
      })
    ) {
      console.log(passList);
      await curUser.updateOne({ passList: passList });
      return res.status(200).json("Deleted!");
    } else {
      console.log(passList);
      return res.status(203).json("No password found!");
    }
  } catch (e) {
    console.error(e);
  }
});

router.post("/getpass", async (req, res) => {
  const { username, password } = req.body;
  console.log(username, password);
  try {
    const exists = await user.findOne({ username: username });
    if (exists) {
      let auth = await bcrypt.compare(password, exists.password);
      if (auth) {
        let passList = exists.passList;
        const passwords = [];
        passList.forEach((e) => {
          e.pass = decrypt(e.pass);
          passwords.push(e);
          console.log(e.pass);
        });
        return res.status(200).json(passwords);
      } else {
        return res.status(403).json("Incorrect password");
      }
    }
  } catch (e) {
    console.error(e);
  }
  return res.status(400).json("Bad Request");
});

router.post("/login", async (req, res) => {
  const { username, password } = req.body;
  try {
    const exists = await user.findOne({ username: username });
    console.log(exists);
    if (exists) {
      let auth = await bcrypt.compare(password, exists.password);
      if (auth) {
        return res.status(200).json("Successfully logged in!");
      } else {
        return res.status(403).json("Incorrect password");
      }
    }
  } catch (e) {
    console.error(e);
  }
  return res.status(404).json("User not found!");
});

router.post("/register", async (req, res) => {
  // Request body is still empty :(
  console.log("Adding user");
  const { username, password } = req.body;
  console.log(username + password);
  try {
    let exists = await user.findOne({ username: username });
    console.log(exists);
    if (exists === null) {
      const saltRounds = 10;
      let hashed = await bcrypt.hash(password, saltRounds);
      let newUser = new user({
        username: username,
        password: hashed,
        passList: [],
      });
      console.log(newUser);
      await newUser.save();
      return res.status(200).json({ msg: "Added user" });
    }
    return res.status(403).json({ err: "Username already exists" });
  } catch (e) {
    console.error(e);
    return res.status(404);
  }
});

module.exports = router;
